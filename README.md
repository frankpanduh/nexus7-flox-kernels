# nexus7-flox-kernels

Nexus 2013 Kernel IMG backups.


## Description
Collection / Backup of IMG and kernel tools for Nexus 2013 (flox).


## Roadmap
Archive kernel tools and kernel images for development of Nexus 7 (2013) which is no longer updated or maintained by Asus / Google.
<br>
To help push kernel development for Nexus 7 (2013).

## Contributing
Feel free to contribute. I am just a dabbler in android development by no means an expert. 


